# DelMar MacOS VPN Setup

## Requirements

1. [Tunnelblick](https://tunnelblick.net), an OpenVPN client for MacOS.
1. A `.tblk` file, provided by an official DelMar person.

## Installation

1. Install Tunnelblick
2. Double-click on the `.tblk` package. Tunnelblick should try to open it as a VPN configuration.
  ![Config Install 1](./readme-images/config-install-1.png)
  ![Config Install 2](./readme-images/config-install-2.png)
  ![Config Install 3](./readme-images/config-install-3.png)

3. When the configuration is installed, connect to the VPN connection using the Tunnelblick menu item

  ![Connect 1](./readme-images/connect-1.png)
  ![Connect 2](./readme-images/connect-2.png)
  ![Connect 3](./readme-images/connect-3.png)
